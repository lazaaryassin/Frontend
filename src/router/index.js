import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import SharedwithmeView from '../views/SharedwithmeView.vue'
import Organization from '../views/OrganizationView.vue'
import MydocumentsView from '../views/MydocumentsView.vue'
import CommunitieView from '../views/CommunitieView.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/Home',
    name: 'Home',
    meta: {layout: "AppLayout"},
    component: HomeView
  },
  {
    path: '/Mydocuments',
    name: 'Mydocuments',
    component: MydocumentsView,
  },
    {
      path: '/Sharedwithme',
      name: 'Shared with me',
      component:SharedwithmeView,

    },
    {
      path: '/login',
      name: 'login',
      meta: {layout: "RegisterLayout"},
      component:Organization,

    },
{
    path: '/Communitie',
    name: 'Communitie',
    component:CommunitieView,

},

    {
    path: '/user',
    name: 'user',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/OrganizationView.vue')
    }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
